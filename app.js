var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var faker  = require("faker");
var builder = require('xmlbuilder');
var fs = require("fs");

var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json());

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'address',
  database : 'mydb'
});
connection.connect(
  (err)=>{
    if(err)
      console.log("Unable to connect");
    console.log("connection sucessful");
    // seedDB(connection);
  }
);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/users', usersRouter);


/* Routes */
app.get('/', function(req, res, next) {
  res.render('index', { title: 'Billing System' ,site1:null,site2:null, site3 : null ,site4 : null});
});
app.get('/create',(req,res)=>{
  res.render('create');
});
app.post('/create',(req,res)=>{
  let sql = "INSERT INTO `prod` SET ?"
  var prod = {};
  prod.name = req.body.name;
  prod.site = req.body.site;
  prod.price = req.body.price;
  prod.qty = req.body.qty;
  connection.query(sql,prod,(err)=>{
    if(err)
        return res.redirect("/");
      else{
        return res.redirect("/");
      }

  });

});
app.get('/vrFrag',(req,res)=>{
  let sql1 = "SELECT * FROM site3";
  let sql2 = "SELECT * FROM site4";
  var prod = {};
  var site3 = null;
  var site4 = null;
  connection.query(sql1,(err,result3)=>{
    if(err)
        return res.redirect("/");
      else{
        site3 = result3;
        connection.query(sql2,(err,result4)=>{
          if(err)
            return res.redirect("/");
            else{
              site4 = result4;
              res.render("index",{title: 'Billing System', site1:null,site2:null,site3 : site3 ,site4:site4});
            }
        });
      }
  });
});


app.get('/hrFrag',(req,res)=>{
  let sql1 = "SELECT * FROM site1";
  let sql2 = "SELECT * FROM site2";
  var site1 = null;
  var site2 = null;
  connection.query(sql1,(err,result1)=>{
    if(err)
        return res.redirect("/");
      else{
        site1= result1;
        connection.query(sql2,(err,result2npm)=>{
          if(err)
              return res.redirect("/");
            else{
              site2 = result2;
              res.render("index",{title: 'Billing System',site1 : site1,site2:site2 ,site3 : null ,site4:null });
            }
        });
      }
  });
});

app.get('/xmlTree',(req,res)=>{
  let sql1 = "SELECT * FROM prod";

  connection.query(sql1,(err,result)=>{
    if(err)
      return res.redirect("/");
    else{
      var root= {
        products : {
        product:result
      }
      };
      var xml = builder.create(root, { encoding: 'utf-8' });
      fs.writeFile("products.xml",xml);
      res.set('Content-Type', 'text/xml');
      res.send(xml.end({ pretty: true }));
    }
  });
  

});


// app.get('/table',(req,res)=>{
//   let sql = 'CREATE TABLE `mydb`.`site2` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `price` VARCHAR(255) NOT NULL , `qty` VARCHAR(255) NOT NULL,`site` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB);
//   connection.query(sql,(err,result)=>{
//     if(err)
//       console.log(err);
//       else {
//         console.log("Table created sucessfull ");
//         res.send("Sucessfull");

//       };
//   });
// });

/*End*/ 
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



function seedDB(conn){

var site = ["Bandra" , "Vashi"];


  for(var i=0;i<100;i++){
    let sql = "INSERT INTO `prod` SET ?"
  var prod = {};
    curSite = site[0]; 
    var bool = faker.random.boolean();
    if(bool)
    {
      curSite = site[1]; 
    }
  prod.name = faker.commerce.productName();
  prod.site = curSite;
  prod.price = faker.commerce.price();
  prod.qty = faker.random.number(100);
  connection.query(sql,prod,(err)=>{
    if(err)
        console.log("Unsucessfull");
  });
  }
}


module.exports = app;


